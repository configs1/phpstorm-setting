#!/bin/bash
#rm -rf *
declare -A modulesToInstall
modulesToInstall[Base]="git@git.amasty.com:magento2/base.git"
modulesToInstall[Shopby]="git@git.amasty.com:magento2/shop-by.git"
modulesToInstall[ShopbyBase]="git@git.amasty.com:magento2/shop-by-base.git"
modulesToInstall[ShopbyBrand]="git@git.amasty.com:magento2/shop-by-brand.git"
modulesToInstall[ShopbySeo]="git@git.amasty.com:magento2/shop-by-seo.git"
modulesToInstall[ShopbyPage]="git@git.amasty.com:magento2/shop-by-page.git"

for i in "${!modulesToInstall[@]}"
do
	echo ${modulesToInstall[$i]} $i
	git clone ${modulesToInstall[$i]} $i
	cd $i
	git checkout $1 || git checkout dev || git checkout master
	cd ../
done
